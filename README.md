# UCBL-TIME-FILTERING PACKAGE
In this package, you will find:

The filter Filter.py file contains definition for various time filtering algorithms (ex : Average, Median,etc...).
The main.py file is the entry point of the program. It parses command arguments, and [load => filter => write] a video file accordingly.

Accordingly to this package, we provides some usage of this package :
`python3 -m ucbl_time_filtering -i path/to/input.mp4 -o path/to/output.mp4 "Average(10)" (average filter, kernel=10 )`
This command lauch the filtering of an video locate on "path/to/output" with the Average function with a kernel with size of 5.
We can have anothers examples:
`python3 -m ucbl_time_filtering -i path/to/input.mp4 -o path/to/output.mp4 "Gaussian(60,5)" (average filter, kernel=60, sigma=5)`
