import numpy as np
import scipy.stats as stats


class Conv1D(object):
    def __init__(self, kernel):
        self.size = len(kernel)
        self.kernel = kernel

    def generate(self, frame_generator):
        buffer = []
        shape = None
        bit_depth = None,

        while True:
            while len(buffer) < self.size:
                buffer.append(np.float32(next(frame_generator)))

            if shape is None:
                shape = buffer[0].shape
            if bit_depth is None:
                bit_depth = buffer[0].depth()

            frame = np.zeros(shape, np.float32)
            for i in range(self.size):
                frame += buffer[i] * self.kernel[i]

            buffer.pop(0)
            yield np.uint8(frame)


class Median(object):
    def __init__(self, size):
        self.size = size

    def generate(self, frame_generator):
        buffer = []
        shape = None
        bit_depth = None,

        while True:
            while len(buffer) < self.size:
                buffer.append(np.float32(next(frame_generator)))

            if shape is None:
                shape = buffer[0].shape
            if bit_depth is None:
                bit_depth = buffer[0].depth()

            frame = np.median(np.asarray(buffer), axis=0)

            buffer.pop(0)
            yield np.uint8(frame)


class Average(Conv1D):
    def __init__(self, size):
        Conv1D.__init__(self, [1/size for _ in range(size)])


class Gaussian(Conv1D):
    def __init__(self, size, sigma):
        x = np.linspace(-1, 1, size)
        kernel = stats.norm.pdf(x, 0, sigma)
        kernel /= np.sum(kernel)

        Conv1D.__init__(self, kernel)


def factory(definition):
    return eval(definition)
