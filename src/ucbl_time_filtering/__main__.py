import sys, getopt


from ucbl_video_io import VideoReader, VideoWriter
from ucbl_time_filtering import factory


def usage():
    print("Usage: \n -i, --input    select a file as input (image or video). \n -o, --output\
    the recipient file.\n -d        Debug mod.\n And, specified the filter you want with the size of the kernel in quotation marks. \n Filters : \n\
      Average(size)\n      Median(size)\n      Gaussian(size,*optional* sigma)\n\n\
example : python3 -m ucbl_space_filtering -i myVideo.mp4 -o output.mp4 \"Average(10)\"" )


def read_args():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hg:i:o:d", ["help", "input=", "output="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    input_file_arg = None
    output_file_arg = None

    for op, arg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op == '-d':
            global _debug
            _debug = 1
        elif op in ("-i", "--input"):
            input_file_arg = arg
        elif op in ("-o", "--output"):
            output_file_arg = arg

    return input_file_arg, output_file_arg, args


def main():
    if_arg, of_arg, filters_arg = read_args()
    print(if_arg)
    print(of_arg)
    print(filters_arg)

    if if_arg is None:
        raise AttributeError("Missing input (-i <input path>)/(--input=<input path>) argument")
    if of_arg is None:
        raise AttributeError("Missing output (-o <output path>)/(--output=<output path>) argument")

    if len(filters_arg) == 0:
        AttributeError("Missing temporal filter definition")
    if len(filters_arg) != 1:
        AttributeError("Only one temporal filter allowed per run")

    time_filter = factory(filters_arg[0])

    with VideoReader(if_arg) as input_file:
        with VideoWriter(of_arg, input_file.get_meta()) as output_file:
            for frame in time_filter.generate(input_file.read()):
                output_file.write(frame)


if __name__ == "__main__":
    # execute only if run as a script
    main()
